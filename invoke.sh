#!/bin/bash
#commit 1
#issue 2
python3 test.py
EXIT_CODE_PY=`echo $?`
FOLDER_PATH=/tmp/mozilla_ubuntu0/
FILE_PATH=/tmp/mozilla_ubuntu0/*.part.ics

while [ 1 ]
do
   if [[ -d "$FOLDER_PATH" || -f "$FILE_PATH" ]]
   then
       break
   fi
   echo waiting..

   sleep 1s
done

if [ $EXIT_CODE_PY == 0 ]
then
    ./test.sh
else
    echo 'Exit code != 0 ~ test.py'
fi
