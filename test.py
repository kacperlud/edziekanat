from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import json

try:
   fireFoxOptions = Options()
   fireFoxOptions.headless = True
   fireFoxOptions.set_preference("browser.download.panel.shown", False)
   fireFoxOptions.set_preference("browser.download.manager.showWhenStarting", False)
   fireFoxOptions.set_preference("browser.helperApps.neverAsk.openFile","text/calendar")
   fireFoxOptions.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/calendar")

   browser = webdriver.Firefox(options=fireFoxOptions)
   data = json.load(open('../ppp.json'))

   username = data['username']
   password = data['password']
   plan_page = data['plan_page']

   browser.get (plan_page)
   browser.find_element_by_name("ctl00$ctl00$ContentPlaceHolder$MiddleContentPlaceHolder$txtIdent").send_keys(username)
   browser.find_element_by_name("ctl00$ctl00$ContentPlaceHolder$MiddleContentPlaceHolder$txtHaslo").send_keys(password)
   browser.find_element_by_name("ctl00$ctl00$ContentPlaceHolder$MiddleContentPlaceHolder$butLoguj").click()
   browser.find_element_by_css_selector("input[type='radio'][value='Semestralnie']").click()
   browser.find_element_by_name("ctl00$ctl00$ContentPlaceHolder$RightContentPlaceHolder$btn_DrukujICS").click()
#   print(browser.page_source)
finally:
    try:
        browser.close()
    except:
        pass
