#!/bin/bash
FIRST_FILE=`ls /tmp/mozilla_ubuntu0/ | head -n 1`
FIRST=`md5sum ~/edziekanat/first.ics | awk '{ print $1 }'`
SECOND=`md5sum /tmp/mozilla_ubuntu0/"$FIRST_FILE" | awk '{ print $1 }'`

if [ "$FIRST" == "$SECOND" ]
then
  echo same
  rm -f /tmp/mozilla_ubuntu0/*
elif [ "$FIRST" != "$SECOND" ]
then
  echo not the same
  mv "$SECOND" ~/edziekanat/to_download.ics
fi


